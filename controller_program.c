#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
//Define functions
//======================
void ioinit(void);
void led_on(void);
void led_off(void);
//======================
//This function is used to initialize the USART
//at a given UBRR value
void USARTInit(uint16_t ubrr_value)
{

   //Set Baud rate

   UBRRL = ubrr_value;
   UBRRH = (ubrr_value>>8);

   /*Set Frame Format


   >> Asynchronous mode
   >> No Parity
   >> 1 StopBit

   >> char size 8

   */

   UCSRC=(1<<URSEL)|(3<<UCSZ0);


   //Enable The receiver and transmitter

   UCSRB=(1<<RXEN)|(1<<TXEN);


}


//This function is used to read the available data
//from USART. This function will wait untill data is
//available.
char USARTReadChar()
{
   //Wait untill a data is available

   while(!(UCSRA & (1<<RXC) ))
   {
      //Do nothing
   }
	
   //Now USART has got data from host
   //and is available is buffer

   return UDR;
}


//This fuction writes the given "data" to
//the USART which then transmit it via TX line
void USARTWriteChar(char data)
{
   //Wait untill the transmitter is ready

   while(!(UCSRA & (1<<UDRE)))
   {
      //Do nothing
	 
   }
	
   //Now write the data to USART buffer

   UDR=data;
}

int main (void)
{
	ioinit(); //Setup IO pins and defaults
	//This DEMO program will demonstrate the use of simple

   //function for using the USART for data communication

   //Varriable Declaration
   char data=0;

   /*First Initialize the USART with baud rate = 19200bps
   
   for Baud rate = 19200bps

   UBRR value = 51

   */

   USARTInit(51);    //UBRR = 51
	
   //Loop forever
	while (1)
	{
		//Read data
		
		data=USARTReadChar();
		_delay_ms(1000);
		
		
					//_delay_ms(500);
				led_on();
				for(int i=0;i<120;i++){
					//user pressed the button to close ringing 
					if (bit_is_clear(PINC, 4))
					{
						break;
					}
					_delay_ms(1000);
				}
				led_off();
				
		
	}
}

void ioinit (void)
{
	DDRC  =  0b11101111; //Pin 27 of MCU as input
	PORTC = 0b00010000; //Enable internal pullup of pin 27
}

void led_on(void)
{
	PORTC |= _BV(PC5); //Pin 28 of MCU as output
}

void led_off(void)
{
	PORTC &= ~_BV(PC5);
}