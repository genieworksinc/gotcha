package com.gotcha.finder.gotcha;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import android.widget.EditText;
import android.util.Log;
import android.os.AsyncTask;
import android.content.DialogInterface;
import android.app.AlertDialog;

import com.google.android.gms.maps.GoogleMap;

import java.util.ArrayList;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

public class FindDevice extends Activity  {

    private static final String TAG = "FIND_DEVICE";



    private String provider;

    //bluetooth management class.
    private bluetoothManager btManager;
    //MAcAddress Of Device
    private String MacAddress;
    //boolean value to indicate is device is found or not
    private boolean isConnected=false;
    //Bluetooth Adapter
    private BluetoothAdapter myAdapter;

    private ProgressBar mSpinner;
    //TextView above the spinner
    private TextView searching;
    //Button to show current location map
    private Button currentLocMap;
    //Button to show last seen location map
    private Button lastKnownLocMap;
    
    // User email
    private String email;

    // device name
    private String name;

    private GDevice device;

    private LocationManager locationManager;




    @Override
    public void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_device);

        //TO Know  Bluetooth Condition
        IntentFilter filter1 = new IntentFilter(BluetoothDevice.ACTION_ACL_CONNECTED);
        //IntentFilter filter2 = new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        IntentFilter filter3 = new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED);

        this.registerReceiver(mReceiver, filter1);
        //this.registerReceiver(mReceiver, filter2);
        this.registerReceiver(mReceiver, filter3);

         locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);



        //get an instance of the bluetooth manager
        btManager = bluetoothManager.getInstance();

        //get passed  MacAddress
        device = Globals.searchedForDevice;

        MacAddress = device.getMacAddress();
        name = device.getName();

        mSpinner = (ProgressBar) findViewById(R.id.progressBar);
        mSpinner.setIndeterminate(true);

        searching=(TextView) findViewById(R.id.Searching);

        currentLocMap=(Button) findViewById(R.id.currentLocMap);
        currentLocMap.setVisibility(View.GONE);
        currentLocMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(!isGPSEnabled()){
                   showGPSDisabledAlertToUser();
               }
               else{
                   Globals.current=true;
                   startActivity(new Intent(FindDevice.this,MapsActivity.class));
                   finish();
               }
            }
        });

        lastKnownLocMap=(Button) findViewById(R.id.lastLocMap);
        lastKnownLocMap.setVisibility(View.GONE);
        lastKnownLocMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(device.getLatitude()!=-900) {
                    if (!isGPSEnabled()) {
                        showGPSDisabledAlertToUser();
                    } else {

                        Globals.current = false;
                        startActivity(new Intent(FindDevice.this, MapsActivity.class));
                        finish();
                    }

                }
                else{
                   showNoLastSeenAddressKnown();
                }

            }
        });

        //Drawable image = new BitmapDrawable(DataManager.getInstance().getThumbnail(MacAddress, this));
        Bitmap bitmap = DataManager.getInstance().getThumbnail(MacAddress, this);

        if (bitmap != null) {
            CircleDrawable circle = new CircleDrawable(bitmap, true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                mSpinner.setBackground(circle);
            else {
                mSpinner.setBackgroundDrawable(circle);
            }
        }
        else{
            bitmap= BitmapFactory.decodeResource(this.getResources(),
                    R.drawable.bridge);
            CircleDrawable circle = new CircleDrawable(bitmap, true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                mSpinner.setBackground(circle);
            else {
                mSpinner.setBackgroundDrawable(circle);
            }
        }


        // connect to the device
        btManager.connectToDevice(MacAddress);



        new CountDownTimer(5000, 2000) {

            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                if (!isConnected) {

                    device.setLost(true);
                    mSpinner.setVisibility(View.GONE);
                    //searching.setVisibility(View.GONE);
                    searching.setText("Device not found");
                    lastKnownLocMap.setVisibility(View.VISIBLE);

                    if (!device.isMarkedAsLost()) {
                        reportDeviceAsLost();
                    }
                }
            }
        }.start();



        //Get Bluetooth Adapter from Bluetooth Manager
        myAdapter=bluetoothManager.getInstance().getMyBluetoothAdapter();

        this.registerReceiver(mReceiver2, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
    }


    @Override
    protected void onResume() {
        super.onResume();

    }


    @Override
    protected void onPause() {
        super.onPause();
        //btManager.turnDeviceOFF(MacAddress);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
        unregisterReceiver(mReceiver2);

        // connect to the device
        btManager.closeCurrentConnection();
    }

    //The BroadcastReceiver that listens for bluetooth broadcasts
    private final BroadcastReceiver mReceiver  = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();


        if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
            //Do something if connected

            mSpinner.setVisibility(View.GONE);
           // searching.setVisibility(View.GONE);
            searching.setText("Device is ringing just follow the sound");
            currentLocMap.setVisibility(View.VISIBLE);
             setLastSeenLonLag();
            //Device is found
            isConnected=true;

            // clear the lost flag
            if (device.isLost())
                device.setLost(false);

            // un-mark the lost device
            if (device.isMarkedAsLost())
                new DeleteLostDeviceTask().execute(device.getMacAddress());
        }
        }
    };
    /*
        set the last seen location to the current longitude and latitude
     */
    private void setLastSeenLonLag() {
        // Create a criteria object to retrieve provider
        Criteria criteria = new Criteria();

        // Get the name of the best provider
        String provider = locationManager.getBestProvider(criteria, true);

        // Get Current Location
        Location myLocation = locationManager.getLastKnownLocation(provider);

        // Get latitude of the current location
        double latitude = myLocation.getLatitude();

        // Get longitude of the current location
        double longitude = myLocation.getLongitude();

        device.setLatitude(latitude);
        device.setLongitude(longitude);
    }


    /**
     * Check if bluetooth was turned off during the app
     */
    private final BroadcastReceiver mReceiver2 = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();

            // It means the user has changed his bluetooth state.
            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {

                if (myAdapter.getState() == BluetoothAdapter.STATE_TURNING_OFF) {
                    // The user bluetooth is turning off yet, but it is not disabled yet.
                    return;
                }

                if (myAdapter.getState() == BluetoothAdapter.STATE_OFF) {
                    // The user bluetooth is already disabled.
                    Toast.makeText(getApplicationContext(), "Gotcha is closed your Bluetooth is turned off",
                            Toast.LENGTH_LONG).show();
                    finish();
                }

            }
        }
    };
    
    void reportDeviceAsLost() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set title
        alertDialogBuilder.setTitle("Lost your things!");

        // set dialog message
        alertDialogBuilder
                .setMessage("you can mark this item as lost, to get help finding it")
                .setCancelable(false)
                .setPositiveButton("mark as lost",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {

                        markAsLost();
                    }
                })
                .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {

                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    /**
     * Mark user device as lost
     */
    private void markAsLost(){

        final AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Email");
        alert.setMessage("supply your email, so you can get a notification that it was found");


        // Set an EditText view to get user input
        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                email = input.getText().toString();
                if (email != null) {
                    device.setEmail(email);
                    // TODO: supply the correct last seen address
                    new AddLostDeviceTask().execute(email, MacAddress, name, "12");
                }
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
            }
        });

        alert.show();
    }

    /**
     * Add a device to the missing list.
     */
    private class AddLostDeviceTask extends AsyncTask<String, Void, Boolean> {

        private final String TAG = "ADD_LOST_TASK";

        @Override
        protected Boolean doInBackground(String... params) {
            return (ClientManager.isConnected() || ClientManager.enableWifi()) && addLostDevice(params[0], params[1], params[2], params[3]);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            // show result of success

            if (result) {
                Toast.makeText(getApplicationContext(), name + " marked as lost", Toast.LENGTH_SHORT).show();
                device.setMarkedAsLost(true);
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "please try again!", Toast.LENGTH_SHORT).show();
            }
        }

        /**
         * Report a device was lost.
         * @param address mac-address of the device
         * @param name name of the device
         * @param lastSeenLocation last known location of the device
         * @return true on success, false otherwise
         */
        private boolean addLostDevice(String email, String address, String name, String lastSeenLocation) {
            try {

                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(ClientManager.getServerURL());

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                //put here in nameValuePairs request parameters
                nameValuePairs.add(new BasicNameValuePair("POST_TYPE", "ALD"));
                nameValuePairs.add(new BasicNameValuePair("deviceAddress", address));
                nameValuePairs.add(new BasicNameValuePair("deviceName", name));
                nameValuePairs.add(new BasicNameValuePair("deviceLastAddress", lastSeenLocation));
                nameValuePairs.add(new BasicNameValuePair("email", email));

                UrlEncodedFormEntity form = new UrlEncodedFormEntity(nameValuePairs, "UTF-8");
                form.setContentEncoding(HTTP.UTF_8);
                httppost.setEntity(form);

                HttpResponse response = httpclient.execute(httppost);

                Log.d(TAG, String.valueOf(response.getStatusLine().getStatusCode()));
                int status = response.getStatusLine().getStatusCode();
                if ( status == 200) {
                    Log.d(TAG, "success");
                    return true;
                } else if (status == 500) {
                    Log.d(TAG, "internal server error");
                    return false;
                } else {
                    Log.e(TAG, "bad argument!");
                    return  false;
                }

            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
                return false;
            }
        }
    }

    /**
     * Remove the lost device as not lost.
     */
    private class DeleteLostDeviceTask extends AsyncTask<String, Void, Boolean> {

        private final String TAG = "DELETE_LOST_DEVICE_TASK";

        @Override
        protected Boolean doInBackground(String... params) {
            // check available internet connection.
            return (ClientManager.isConnected() || ClientManager.enableWifi()) && deleteDevice(params[0]);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result)
                device.setMarkedAsLost(false);
        }

        /**
         * Remove a device from the lost list.
         * @param address mac-address of the device
         * @return true on success, false on failure
         */
        public boolean deleteDevice(String address) {
            try {

                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(ClientManager.getServerURL());

                List nameValuePairs = new ArrayList<NameValuePair>();
                //put here in nameValuePairs request parameters
                nameValuePairs.add(new BasicNameValuePair("POST_TYPE", "DLD"));
                nameValuePairs.add(new BasicNameValuePair("deviceAddress", address));

                UrlEncodedFormEntity form = new UrlEncodedFormEntity(nameValuePairs, "UTF-8");
                form.setContentEncoding(HTTP.UTF_8);
                httppost.setEntity(form);

                HttpResponse response = httpclient.execute(httppost);

                Log.d(TAG, String.valueOf(response.getStatusLine().getStatusCode()));
                int status = response.getStatusLine().getStatusCode();
                if ( status == 200) {
                    Log.d(TAG, "Success");
                    return true;
                } else if (status == 500) {
                    Log.e(TAG, "Internal server error");
                    return false;
                } else {
                    Log.e(TAG, "bad argument!");
                    return  false;
                }

            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
                return false;
            }
        }
    }

    private void showGPSDisabledAlertToUser(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Enable GPS");
        alertDialogBuilder.setMessage("Enable GPS to show map")
                .setCancelable(false)
                .setPositiveButton("Enable GPS",
                        new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int id){
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
    /*
        to check if GPS is enable or not
     */
    private boolean isGPSEnabled(){

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
           return true;
        }else{
            return false;
        }
    }

    /*
        alert the user that no last seen address is available (long==-900,latitude==-900)
     */
    private void showNoLastSeenAddressKnown(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Last seen address");
        alertDialogBuilder.setMessage("Soory,last seen address is not available")
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int id){
                                dialog.cancel();
                            }
                        });

        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

}

