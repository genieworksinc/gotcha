package com.gotcha.finder.gotcha;

import android.app.Activity;
import android.os.Bundle;


import java.util.List;

/**
 * Settings activity that controls user preferences
 * Created by mohamed on 6/2/2015
 */
public class Settings extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }
}

