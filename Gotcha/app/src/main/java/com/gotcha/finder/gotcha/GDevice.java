package com.gotcha.finder.gotcha;

/**
 * Represent a Gothca device
 * Created by mohamed on 2/6/15.
 */
public class GDevice {
    private String name;
    private double longitude;
    private double latitude;
    private String macAddress;
    private String email;
    private boolean lost;
    private boolean markedAsLost;

    public GDevice(String name, double longitude,double latitude, String macAddress, String email,
                   boolean lost, boolean markedAsLost) {
        this.name = name;
        this.longitude=longitude;
        this.latitude=latitude;
        this.macAddress = macAddress;
        this.email = email;
        this.lost = lost;
        this.markedAsLost = markedAsLost;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {return latitude;}

    public String getMacAddress() {
        return macAddress;
    }

    public boolean isLost() {
        return lost;
    }

    public boolean isMarkedAsLost() {
        return markedAsLost;
    }

    public void setMarkedAsLost(boolean markedAsLost) {
        this.markedAsLost = markedAsLost;
    }

    public void setLost(boolean lost) {
        this.lost = lost;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setName(String name) {
        this.name = name;
    }
}

