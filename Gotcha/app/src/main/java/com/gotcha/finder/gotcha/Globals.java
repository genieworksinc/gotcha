package com.gotcha.finder.gotcha;

/**
 * Global class to pass objects between different activities and classes.
 * Created by mohamed on 2/12/15.
 */
public class Globals {
    public static GDevice searchedForDevice;
    //show current location or last seen location passed to MapsActivity
    public static boolean current;
}
