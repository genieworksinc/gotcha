package com.gotcha.finder.gotcha;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.provider.MediaStore;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.AbstractMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

public class Main extends ActionBarActivity {

    private static String TAG = "MAIN";
    public static final String PREFS_NAME = "MyPrefsFile";

    //list view of devices.
    private ListView listView;

    //DataManager for data fetching and saving.
    private DataManager dataManager;

    //array adapter associated with list view.
    private  ArrayList<GetDevice> searchResults;
    
    // name of selected device
    private String name;

    //index of selected item in the list view.
    private int selectedItem = -1;

    //MAcAddress to pass to FindDevice Activity
    private String FindMAc;
    //Bluetooth Adapter
    private BluetoothAdapter myAdapter;
    //CustomBaseAdapter
    private MyCustomBaseAdapter myCustomBaseAdapter;
    //take image to change device image
    static final int REQUEST_IMAGE_CAPTURE = 3;
    //a text view that will appear when there is no added devices,to tell the user what to do.
    private TextView NoDevicesText;

    private int notificationID = 555;

    //location manager to get GPS state
    private LocationManager locationManager;


    /**
     * onCreate method
     * @param savedInstanceState saved instance
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Get instance of the DataManger
        dataManager = DataManager.getInstance();
        //Set content view
        setContentView(R.layout.activity_main);

        NoDevicesText = (TextView) findViewById(R.id.welcome);
        //Show if there is no added devices
        if(dataManager.isEmpty()){
            NoDevicesText.setVisibility(View.VISIBLE);
        }
        else{
            NoDevicesText.setVisibility(View.GONE);
        }
       //enable the user to turn on GPS
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if(!isGPSEnabled()){
            showGPSDisabledAlertToUser();
        }
        //Get devices in the dataManager
         searchResults = GetDevices();
        //Get Bluetooth Adapter from Bluetooth Manager
        myAdapter=bluetoothManager.getInstance().getMyBluetoothAdapter();


        listView= (ListView) findViewById(R.id.srListView);

        // initialize the client manager
        ClientManager.init(getApplicationContext());

        myCustomBaseAdapter=new MyCustomBaseAdapter(this,searchResults);
        listView.setAdapter(myCustomBaseAdapter);
        // request creating context menu when user presses an item for a period of time.
        registerForContextMenu(listView);

        this.registerReceiver(mReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));

        // check to enable the lookout mode
        checkLookoutMode();

        // check for any updates about user's lost devices
        checkLostDevices();
    }

    /**
     * Check if a lost device was found or not
     */
    private void checkLostDevices() {
        ArrayList<GDevice> lostDevices = dataManager.getLostDevices();

        for (GDevice device : lostDevices) {
            new CheckMyLostDevicesTask().execute(device);
        }
    }

    /**
     * Load the user settings for the lookout mode
     */
    private void checkLookoutMode() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        boolean lookoutMode = settings.getBoolean("lookout_mode", true);

        Log.d(TAG, String.valueOf(lookoutMode));

        if (lookoutMode) {
            lookOutForDevices();
        }
    }

    /**
     * lookout for nearby devices and report them
     */
    private void lookOutForDevices() {
        new LookoutForDevices().execute(getApplicationContext());
    }

    @Override
    protected void onResume() {
        super.onResume();

        //reload the list items as it was changed.
        if (!dataManager.isLoaded()) {
            notifyDataChanged();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");

        //save data to internal storage
        dataManager.save(getApplicationContext());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
        Log.d(TAG, "onDestroy");
    }

    /**
     * Notify the array adapter that the data set was changed.
     */
     private void notifyDataChanged() {
         dataManager.setLoaded();
         ArrayList<GetDevice> newList;
         newList=GetDevices();
         myCustomBaseAdapter.updateList(newList);

         //To mke sure that the textView will not show if is an added device
         if(dataManager.isEmpty()){
             NoDevicesText.setVisibility(View.VISIBLE);
         }
         else{
             NoDevicesText.setVisibility(View.GONE);
         }
    }

    /**
     * Menu that appears if an item in the listView is click for long time.
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        AdapterView.AdapterContextMenuInfo aInfo = (AdapterView.AdapterContextMenuInfo) menuInfo;

        //store the position of selected item for contextMenu
        selectedItem = aInfo.position;
        Log.d(TAG, String.valueOf(selectedItem));
        //create menu
        menu.setHeaderTitle("Options");
        menu.add(1, 1, 1, "Find");
        menu.add(1, 2, 2, "Change name");
        menu.add(1,3,3,"Take a new image");
        menu.add(1,4,4, "Remove");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        //1. poke device, 2. edit device, 3. delete device
        if (itemId == 1) {
            //poke device to enable it

            //TODO: now we use Globals
            FindMAc=dataManager.getAddress(selectedItem);
            name = dataManager.getName(selectedItem);

            //open FindDevice Activity
            findDevice();
            return  true;
        } else if (itemId == 2) {
            //edit name
            editDeviceName();
            return true;
        }else if(itemId==3) {
            //take a new image
            dispatchTakePictureIntent();
        }else if (itemId == 4) {

            deleteDevice(this);
            return true;
        }

        return  super.onContextItemSelected(item);
    }

    /**
     * Menu that appears when option button is clicked.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Log.d(TAG, String.valueOf(id));
        if (id == R.id.action_addDevice) {
            //start the add device activity
            addDevice();
            finish();
            return true;
        } //else if (id == R.id.action_settings) {
            //settings();
        //}
        
        //TODO: to be supported the about screen
        return super.onOptionsItemSelected(item);
    }
    
    /**
    * Start the settings and user preferences activity
    */
    private void settings() {
        Intent i = new Intent(this, Settings.class);
        startActivity(i);
	}

    /**
     * Starts the addDevice activity.
     */
    private void addDevice() {
        Intent i = new Intent(this, AddDevice.class);
        startActivity(i);
    }

    /**
     * Starts the FindDevice activity.
     */
    private void findDevice(){
        Intent i = new Intent(this, FindDevice.class);
        Globals.searchedForDevice = dataManager.getDevice(selectedItem);
        startActivity(i);
    }

    /*
        Edit device name
     */
    private void editDeviceName() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Edit Name");
        alert.setMessage("Enter device name");


        // Set an EditText view to get user input
        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String name = input.getText().toString();
                if(!name.equals("")) {
                    dataManager.editName(selectedItem, name);
                    notifyDataChanged();
                }
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
            }
        });

        alert.show();
    }
    /*
        delete device from the list
     */
    private void deleteDevice(final Activity a){

        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        String deletedDevice = dataManager.getName(selectedItem);
        alert.setTitle("Remove "+ deletedDevice);
        alert.setMessage("Are you sure you want to remove your "+ deletedDevice);

        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //delete img
                dataManager.deleteImgFromInternalStorage(dataManager.getAddress(selectedItem),a);
                //delete device
                dataManager.deleteDevice(selectedItem);
                //refresh list items
                notifyDataChanged();
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
            }
        });

        alert.show();

    }

    /**
     *  Get list of devices from DataManager and give them to the list view
     * @return ...
     */
    private ArrayList GetDevices(){
        //get names of devices in the DataManager
        ArrayList<String> list = new ArrayList<String>();
        dataManager.getNames(list);
        //Devices to be returned
        ArrayList results = new ArrayList();


        int i=0;
        for (String s : list){
            GetDevice sr1 = new GetDevice();
             sr1.setName(s);
            Bitmap deviceImage=dataManager.getThumbnail(dataManager.getAddress(i++), this);
            if(deviceImage==null) {
                //add image to the device TODO add Gotcha icon instead
                deviceImage = BitmapFactory.decodeResource(this.getResources(),
                        R.drawable.bridge );
            }
             sr1.setIcon(deviceImage);
             results.add(sr1);

        }
        dataManager.setLoaded();

        return results;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (selectedItem != -1) {
            if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
                String Address=dataManager.getAddress(selectedItem);
                //delete previous image from internal storage
                dataManager.deleteImgFromInternalStorage(Address,this);
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                //save new image to internal storage
                dataManager.saveImageToInternalStorage(imageBitmap,Address,this);
                //update listView
                notifyDataChanged();

            }
        }
    }

    /**
     * Check if bluetooth was turned off during the app
     */
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();

            // It means the user has changed his bluetooth state.
            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {

                if (myAdapter.getState() == BluetoothAdapter.STATE_TURNING_OFF) {
                    // The user bluetooth is turning off yet, but it is not disabled yet.
                    return;
                }

                if (myAdapter.getState() == BluetoothAdapter.STATE_OFF) {
                    // The user bluetooth is already disabled.
                    Toast.makeText(getApplicationContext(), "Gotcha is closed your Bluetooth is turned off",
                            Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        }
    };

    /*
        Take image by camera to change device image
     */
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    /**
     * Show a notification message with a alarm sound about an update for a lost device
     * @param devices that was found by other users
     */
    private void showNotification(ArrayList<Map.Entry<String, String> > devices) {
        Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mBuilder;
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        for (Map.Entry<String, String> entry : devices) {
            mBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setContentTitle(entry.getKey() + " was found!")
                    .setContentText("your device that was reported lost was seen near "  + entry.getValue())
                    .setSound(uri);

            // notificationID allows you to update the notification later on.
            mNotificationManager.notify(notificationID++, mBuilder.build());
        }
    }

    /**
     * Checks for any updates about user's lost devices.
     */
    private class CheckMyLostDevicesTask extends AsyncTask<GDevice, Void, Boolean> {

        private final String TAG = "Check_Lost_Devices_Task";
        private GDevice device;
        private JSONArray names;
        private JSONArray lastSeen;

        @SafeVarargs
        @Override
        protected final Boolean doInBackground(GDevice... params) {
            assert params != null;
            Log.d(TAG, "doInBackground");
            // check available internet connection
            device = params[0];
            return (ClientManager.isConnected() || ClientManager.enableWifi()) && checkLostDevice(device.getEmail());
        }

        @Override
        protected void onPostExecute(Boolean result) {
            // show result of success
            if (result) {
                ArrayList<Map.Entry<String, String> > devices = new ArrayList<Map.Entry<String, String> >();
                Map.Entry<String, String> entry;
                for (int i=0; i<names.length(); i++) {
                    try {
                        entry = new AbstractMap.SimpleEntry<String, String>(names.getString(i), lastSeen.getString(i));
                        devices.add(entry);
                    } catch (JSONException e) {
                        Log.w(TAG, e.getMessage());
                    }
                }
                showNotification(devices);
            }
        }

        private boolean checkLostDevice(String email) {
            try {

                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(ClientManager.getServerURL());

                List nameValuePairs = new ArrayList<BasicNameValuePair>();
                //put here in nameValuePairs request parameters
                nameValuePairs.add(new BasicNameValuePair("POST_TYPE", "CLD"));
                nameValuePairs.add(new BasicNameValuePair("email", email));

                UrlEncodedFormEntity form = new UrlEncodedFormEntity(nameValuePairs, "UTF-8");
                form.setContentEncoding(HTTP.UTF_8);
                httppost.setEntity(form);


                HttpResponse response = httpclient.execute(httppost);

                Log.d(TAG, String.valueOf(response.getStatusLine().getStatusCode()));
                int status = response.getStatusLine().getStatusCode();
                if ( status == 200) {
                    Log.d(TAG, "Success");
                    // get the updated last seen address
                    HttpEntity entity = response.getEntity();
                    String responseString = EntityUtils.toString(entity, "UTF-8");

                    Log.d(TAG, responseString);
                    JSONObject jsonObject = new JSONObject(responseString);
                    names = jsonObject.getJSONArray("names");

                    // no updates received about lost devices
                    if (names.length() == 0) {
                        return false;
                    }

                    lastSeen = jsonObject.getJSONArray("lastSeen");
                    return true;
                } else if (status == 500) {
                    Log.e(TAG, "Internal server error");
                    return false;
                } else {
                    Log.e(TAG, "bad argument!");
                    return  false;
                }

            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
                return false;
            }
        }
    }

    private void showGPSDisabledAlertToUser(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Enable GPS");
        alertDialogBuilder.setMessage("allow Gotcha to get your location")
                .setCancelable(false)
                .setPositiveButton("Enable GPS",
                        new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int id){
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
    /*
        to check if GPS is enable or not
     */
    private boolean isGPSEnabled(){

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            return true;
        }else{
            return false;
        }
    }
}

