package com.gotcha.finder.gotcha;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

/**
 * Manages internal storage files for the application.
 * Created by mohamed on 19/08/2014.
 */
public class DataManager {

    /**
     * Gets the dataManager instance.
     * @return dataManager object
     * @throws IllegalAccessError if DataManager.init wasn't called before that call
     */
    public static DataManager getInstance() throws  IllegalAccessError {
        if (!isInitialized) {
            Log.e(TAG, "instance failed.");
            throw new IllegalAccessError("can't access unintialized dataManager");
        }

        Log.d(TAG, "returning instance.");
        return dataManager;
    }

    /**
     * Initializes the dataManager with resources it use.
     * @param context application context.
     */
    public static void init(final Context context) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "init begin..");

                if (!isInitialized) {
                    try {
                        //open file from internal storage
                        FileInputStream inputStream = context.openFileInput("gotcha");
                        Log.d(TAG, "opened file");

                        String line;
                        //read file line by line
                        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                        //4 lines per device turn 1,2,3,4. 5
                        int turn=1;

                        ArrayList<GDevice> devices = new ArrayList<GDevice>();
                        String name = "untitled", address = "NULL", email = "NONE";
                        double longitude=0, latitude=0;
                        boolean lost = false, markedAsLost;
                        GDevice device;

                        while ((line = reader.readLine()) != null) {
                            if (turn==1) {
                                name = line;
                            } else if(turn==2){
                                address = line;
                            } else if(turn==3) {
                                longitude = Double.parseDouble(line);
                            }  else if(turn==4) {
                                latitude = Double.parseDouble(line);
                            } else if (turn == 5) {
                                email = line;
                            } else if(turn == 6){
                                lost = line.equals("true");
                            } else {
                                markedAsLost = line.equals("true");

                                // make a device
                                device = new GDevice(name, longitude,latitude, address, email, lost, markedAsLost);
                                devices.add(device);

                                turn=0;
                            }
                            turn++;

                            Log.d(TAG, "inFile: " + line);
                        }

                        //construct the instance with loaded data
                        dataManager = new DataManager(devices);

                        reader.close();
                    } catch (FileNotFoundException e) {
                        Log.w(TAG, "file not found");
                        dataManager = new DataManager();
                    } catch (IOException e) {
                        Log.e(TAG, e.getMessage());
                    } finally {
                        isInitialized = true;
                        Log.d(TAG, "init ends..");
                    }
                }
            }
        }).start();
    }

    /**
     * Constructs a dataManager with loaded devices
     * @param devices array of devices registered
     */
    private DataManager(ArrayList<GDevice> devices) {
        this.devices = devices;
    }

    /**
     * Constructs a new dataManager with no data from internal storage.
     */
    private DataManager() {
        //this.addresses = new ArrayList<String>();
        //this.names = new ArrayList<String>();
        //this.LastKnownAddress=new ArrayList<String>();
        //this.isLost=new ArrayList<Boolean>();
        this.devices = new ArrayList<GDevice>();
        this.isLoaded = false;
    }

    /**
     * Fills the given list with device names
     */
    public void getNames(ArrayList<String> names) {
        this.isLoaded = true;
        for (GDevice device : devices) {
            names.add(device.getName());
        }
    }

    /**
     * Gets address specified by its index.
     * @param i index in array.
     * @return address as string.
     */
    public String getAddress(int i) {
        return this.devices.get(i).getMacAddress();
    }

    public ArrayList<GDevice> getLostDevices() {
        ArrayList<GDevice> lostDevices = new ArrayList<GDevice>();
        for (GDevice device : devices) {
            if (device.isMarkedAsLost()) {
                lostDevices.add(device);
            }
        }

        return lostDevices;
    }

    /**
     * Get name of a device
     * @param i index in array.
     * @return name as string.
     */
    public String getName(int i) {
        return this.devices.get(i).getName();
    }

    public GDevice getDevice(int i) {
        return this.devices.get(i);
    }

    /**
     * Adds a new device to in memory list.
     * @param device device to be added
     */
    public void addDevice(GDevice device) {
        this.devices.add(device);
        this.isLoaded = false;
    }

    /**
     * Check the exist of a device identified with its mac-address in the data.
     * @param address mac address of device
     * @return true if device is saved, false if new one
     */
    public boolean checkForDevice(String address) {
        for (GDevice device : devices) {
            if (device.getMacAddress().equals(address))
                return true;
        }

        return false;
    }

    /**
     * Remove specific device.
     * @param i index of device to be deleted.
     */
    public void deleteDevice(int i) {
        this.devices.remove(i);
        this.isLoaded = false;
    }

    /**
     * Rename a saved entry.
     * @param index of the edited item
     * @param name new name
     */
    public void editName(int index, String name) {
        devices.get(index).setName(name);
        isLoaded = false;
    }

    /**
     * Save data to internal storage
     */
    public void save(Context context) {
        Log.d(TAG, "saving changes..");
        //MODE_PRIVATE will create the file (or replace a file of the same name)
        try {
            FileOutputStream outputStream = context.openFileOutput("gotcha", Context.MODE_PRIVATE);
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));

            //write data to internal storage
            for (GDevice device : devices) {
                writer.write(device.getName());
                //Log.d(TAG, "saved: " + name);
                writer.newLine();
                writer.write(device.getMacAddress());
                writer.newLine();
                writer.write(String.valueOf(device.getLongitude()));
                writer.newLine();
                writer.write(String.valueOf(device.getLatitude()));
                writer.newLine();
                writer.write(device.getEmail());
                writer.newLine();
                writer.write(String.valueOf(device.isLost()));
                writer.newLine();
                writer.write(String.valueOf(device.isMarkedAsLost()));
                writer.newLine();
            }

            //outputStream.close();
            writer.close();
            Log.d(TAG, "finished saving");
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
        }
    }

    /**
     * Check if data set was loaded before.
     * @return true if it was loaded.
     */
    public boolean isLoaded() {
        return isLoaded;
    }

    /**
     * Check if there is no devices
     * @return true if no devices, false otherwise;
     */
    public boolean isEmpty() {
        return this.devices.isEmpty();
    }

    /**
     * Set the loaded flag by client.
     */
    public void setLoaded() {
        this.isLoaded = true;
    }

    /*
       save image bitMap to internal storage
    */
    public boolean saveImageToInternalStorage(Bitmap image,String name,Activity a) {

        try {
            // Use the compress method on the Bitmap object to write image to
            // the OutputStream
            FileOutputStream fos = a.openFileOutput(name, Context.MODE_PRIVATE);

            // Writing the bitmap to the output stream
            image.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
            Log.d(TAG,"image saved");

            return true;
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
            return false;
        }
    }

    /*
        load image bitMap from internal storage
     */

    public Bitmap getThumbnail(String filename,Activity a) {

        Bitmap thumbnail = null;

        try {
            File filePath = a.getFileStreamPath(filename);
            FileInputStream fi = new FileInputStream(filePath);
            thumbnail = BitmapFactory.decodeStream(fi);
            Log.d(TAG,"image loaded");
        } catch (Exception ex) {
            Log.d(TAG, ex.getMessage());
        }

        return thumbnail;
    }
    /*
        delete img from internal storage
     */
    public boolean deleteImgFromInternalStorage(String imgName,Activity a){
        File dir = a.getFilesDir();
        //get image file from internal memory
        File file = new File(dir, imgName);
        //delete image file from internal memory
        if(file.delete()) {
            Log.d(TAG, "image was deleted");
            return true;
        }
        else{
            Log.d(TAG,"image was not deleted");
            return false;
        }

    }
    ///methods ends.

    //single instance of the dataManager kept inside the class
    private static DataManager dataManager;

    //TAG for debug
    private static String TAG = "DataManager";

    //indicates the initialization state of the class.
    private static boolean isInitialized = false;

    ////non-static members begin:

    //indicates if new changes was loaded;
    private boolean isLoaded;

    private ArrayList<GDevice> devices;

    ////non-static members ends.
}
