package com.gotcha.finder.gotcha;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.Toast;


public class StartActivity extends Activity {
    private int splashTime = 600;
    private Thread thread;
    private ProgressBar mSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_start);

        mSpinner = (ProgressBar) findViewById(R.id.Splash_ProgressBar);
        mSpinner.setIndeterminate(true);

        DataManager.init(getApplicationContext());



        //Turn Bluetooth ON
        if(!bluetoothManager.getInstance().getMyBluetoothAdapter().isEnabled()) {
                turnBluetooth();
        }
        else {
            thread = new Thread(runnable);
            thread.start();
        }
    }
    public Runnable runnable = new Runnable() {
        public void run() {
            try {
                Thread.sleep(splashTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                startActivity(new Intent(StartActivity.this,Main.class));
                finish();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //REQUEST_ENABLE_BT=1 in Bluetooth Manager
        if(requestCode==1&&resultCode==RESULT_CANCELED){
            //USER didn't give permission to turn on Bluetooth
            Toast.makeText(getApplicationContext(), "Gotcha is closed You have to turn Bluetooth on to find your missing objects",
                    Toast.LENGTH_LONG).show();
            finish();
        }
        else if(requestCode==1&&resultCode==RESULT_OK){
            startActivity(new Intent(StartActivity.this,Main.class));
            finish();
        }
    }

    /**
     * Checks the bluetooth is on.
     */
    private void turnBluetooth() {
        bluetoothManager.getInstance().turnBluetoothON(this);
    }


}