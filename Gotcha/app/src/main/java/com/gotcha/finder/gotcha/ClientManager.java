package com.gotcha.finder.gotcha;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

/**
 * Client Services, responsible for checking internet connectivity and enable/disable it
 * as appropriate.
 * Created by mohamed on 1/29/15.
 */
public class ClientManager {

    private static final String url = "http://192.168.1.6:8080/website/BackendServlet";

    private static final String TAG = "CLIENT";

    private static boolean connected = false;

    private static Context appContext;

    /**
     * Is mobile connected to internet
     * @return
     */
    public static boolean isConnected() {
        assert connected == true;
        return connected;
    }

    /**
     * Gets the Server URL.
     * @return server URL
     */
    public static String getServerURL() {
        return url;
    }

    /**
     * Initializes the client class
     * @param context application context of the main activity.
     */
    public static void init(Context context) {
        appContext = context;

        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        // check if you are connected or not
        if (networkInfo != null && networkInfo.isConnected()) {
            Log.d(TAG, "not connected to internet!");
            connected = true;
        } else {
            Log.d(TAG, "connected to internet!");
            connected = false;
        }
    }

    public static boolean enableWifi() {
        WifiManager wifiManager = (WifiManager)appContext.getSystemService(Context.WIFI_SERVICE);

        if (wifiManager != null && wifiManager.setWifiEnabled(true)) {
           Log.d(TAG, "wifi enabled!");
            return true;
       } else {
           Log.d(TAG, "failed to open wifi");
           return false;
        }

    }
}

