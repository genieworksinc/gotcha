package com.gotcha.finder.gotcha;

import android.graphics.Bitmap;
import android.net.Uri;

/*
    This class is to hold  the data of each device in the list view of the main activity
 */
public class GetDevice {
    private String name = "";
   // private Integer icon;
    private Bitmap imgURi;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setIcon(Bitmap icon) {
        this.imgURi = icon;
    }

    public Bitmap getIcon() {
        return imgURi;
    }
}
