package com.gotcha.finder.gotcha;

import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
@TargetApi(Build.VERSION_CODES.ECLAIR)
public final class bluetoothManager {

    private final String TAG = "BT_MANAGER";

    // the thread to manage the currently connected device
    private ConnectThread connectThread = null;

    private static bluetoothManager bluetoothManagerInstance=null;
    private static final int TURNOFF =1 ;
    //private static final int TURNON =0 ;
    //private static final int MESSAGE_READ =2 ;
    private BluetoothAdapter myBluetoothAdapter;
    //private ArrayAdapter<String> mArrayAdapter;
    //public static final UUID MY_UUID= UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");  //This is magic don't ask :)
    Handler mHandler =new Handler(){


        @Override
        public void handleMessage(Message msg){

            super.handleMessage(msg);
            /*
            switch(msg.what) {
                case TURNON: //send 'a' to the device to turn it ON
                    ConnectedThread connectedThread1=new ConnectedThread((BluetoothSocket)msg.obj);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    connectedThread1.write("a".getBytes());
                    connectedThread1.cancel();
                    break;
                case TURNOFF: //Send 'b' to the device to turn it OFF
                    ConnectedThread connectedThread2=new ConnectedThread((BluetoothSocket)msg.obj);
                    connectedThread2.write("b".getBytes());
                    break;
            }
            */
        }

    };

    private bluetoothManager() {
        myBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

    }

    public static bluetoothManager getInstance(){
        if(bluetoothManagerInstance==null)
        {
            bluetoothManagerInstance=new bluetoothManager();
        }
        return bluetoothManagerInstance;
    }


    public void turnBluetoothON(Activity a){
        if (!myBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            a.startActivityForResult(enableBtIntent, 1);
        }

    }
    public BluetoothAdapter getMyBluetoothAdapter() {

        return myBluetoothAdapter;
    }

    /**
     * Connect to a device.
     * @param MacAddress address of the device
     */
    public void connectToDevice(String MacAddress){
        BluetoothDevice device=myBluetoothAdapter.getRemoteDevice(MacAddress);
        Log.d(TAG, device.getName());
        connectThread = new ConnectThread(device);
        connectThread.start();
    }

    /**
     * Sends a message to the currently connected device
     * @param message string to send
     * @return true on success, false otherwise
     */
    public boolean sendMessageTOConnectedDevice(String message) {
        if (null != connectThread) {
            return connectThread.write(message.getBytes());
        }
        return false;
    }

    /**
     * Close the current connection.
     */
    public void closeCurrentConnection() {
        if (null != connectThread) {
            connectThread.cancel();
            connectThread = null;
        }
    }

    public void turnDeviceOFF(String MacAddress){
        BluetoothDevice device=myBluetoothAdapter.getRemoteDevice(MacAddress);
        ConnectThread cThread=new ConnectThread(device);
        cThread.start();

        mHandler.obtainMessage(TURNOFF,cThread.getConnectedSocket());
    }
    //To Check if the device supports Bluetooth or not
    public boolean IsBluetoothIN(){
        return myBluetoothAdapter != null; //if true there is Bluetooth on the device
    }

    //Turn the device bluetooth ON
    public void TurnBluetoothON(Activity a){

        if (!myBluetoothAdapter.isEnabled()) {
            int REQUEST_ENABLE_BT=1;
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            a.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

    }

    //Private class to handle Bluetooth Connection Threads.
    private class ConnectThread extends Thread {

        private final BluetoothSocket mmSocket;
        private OutputStream mmOutStream;

        public ConnectThread(BluetoothDevice device) {
            // Use a temporary object that is later assigned to mmSocket,
            // because mmSocket is final
            BluetoothSocket tmp = null;
            // Get a BluetoothSocket to connect with the given BluetoothDevice
            try {
                Method m = device.getClass().getMethod("createRfcommSocket", new Class[] {int.class});
                tmp = (BluetoothSocket) m.invoke(device, 1);
                Log.d(TAG, "connect thread created RF channel");
            } catch (NoSuchMethodException e) {
                Log.e(TAG, e.getMessage());
            } catch (IllegalAccessException e) {
                Log.e(TAG, e.getMessage());
            } catch (InvocationTargetException e) {
                Log.e(TAG, e.getMessage());
            }
            mmSocket = tmp;
        }

        public void run() {
            Log.d(TAG, "connect thread run");

            // Cancel discovery because it will slow down the connection
            myBluetoothAdapter.cancelDiscovery();

            Log.d(TAG, "connect thread run");
            try {
                // Connect the device through the socket. This will block
                // until it succeeds or throws an exception
                mmSocket.connect();
                Log.d(TAG, "connect thread socket connected");

                mmOutStream = mmSocket.getOutputStream();
                Log.d(TAG, "connect thread outputStream ready");

                // send the letter a to turn on the device
                write("a".getBytes());
            } catch (IOException connectException) {
                // Unable to connect; close the socket and get out
                Log.e(TAG, connectException.getMessage());
                try {
                    mmSocket.close();
                } catch (IOException closeException) {
                    Log.e(TAG, closeException.getMessage());
                }
            }
        }

        /* Call this from the main activity to send data to the remote device */
        public boolean write(byte[] bytes) {
            try {
                if (mmSocket != null) {
                    mmOutStream.write(bytes);
                    Log.d(TAG, "connected thread wrote bytes");
                    return true;
                }
                return false;
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
                return false;
            }
        }

        public BluetoothSocket getConnectedSocket(){

            return mmSocket;
        }

        /** Will cancel an in-progress connection, and close the socket */
        public void cancel() {
            try {
                if(mmOutStream!=null )
                    mmOutStream.close();
                if(mmSocket!=null)
                    mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    /*
    //private class to handle connected Bluetooth Threads.
    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams, using temp objects because
            // member streams are final
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
                Log.d(TAG, "connected thread got stream");
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] buffer = new byte[1024];  // buffer store for the stream
            int bytes; // bytes returned from read()

            // Keep listening to the InputStream until an exception occurs
            while (true) {
                try {
                    // Read from the InputStream
                    bytes = mmInStream.read(buffer);
                    // Send the obtained bytes to the UI activity
                    mHandler.obtainMessage(MESSAGE_READ, bytes, -1, buffer)
                            .sendToTarget();
                } catch (IOException e) {
                    Log.e(TAG, e.getMessage());
                    break;
                }
            }
        }

        /* Call this from the main activity to send data to the remote device
        public void write(byte[] bytes) {
            try {
                mmOutStream.write(bytes);
                Log.d(TAG, "connected thread wrote bytes");
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        }

        /* Call this from the main activity to shutdown the connection
        public void cancel() {
            try {
                mmSocket.close();
                Log.d(TAG, "connected thread closed the socket");
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }
    */
}
