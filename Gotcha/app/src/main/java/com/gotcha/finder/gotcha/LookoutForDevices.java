package com.gotcha.finder.gotcha;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import java.util.ArrayList;
import java.util.List;

/**
 * Search for devices that are not the user's and report them to the application server
 * Created by mohamed on 2/6/15.
 */
public class LookoutForDevices extends AsyncTask <Context, Void, Void> {

    private final static String TAG = "LOOKOUT_DEVICE";

    private final ArrayList nearbyDevices = new ArrayList();

    @Override
    protected Void doInBackground(Context... params) {

        final DataManager dataManager = DataManager.getInstance();
        final bluetoothManager myManager = bluetoothManager.getInstance();
        final BluetoothAdapter bluetoothAdapter = myManager.getMyBluetoothAdapter();
        bluetoothAdapter.startDiscovery();
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                String action = intent.getAction();

                // When discovery finds a device
                if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                    // Get the BluetoothDevice object from the Intent
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                    // add devices that are not registered to this user in the list
                    //  if(device.getName().contains("HC-05")) {
                    if (!dataManager.checkForDevice((device.getAddress()))) {
                        //Log.d(TAG, device.getName());
                        nearbyDevices.add(device.getAddress());
                    }
                    //}
                }
                else if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                    // report the found devices, might be someone lost device
                    Log.d(TAG, "finished searching!");

                    if (!nearbyDevices.isEmpty()) {
                        Log.d(TAG, "found enough data to send to server");
                        new ReportFoundDevicesTask().execute(nearbyDevices);
                    }
                }
            }
        };
        // Register the BroadcastReceiver
        params[0].registerReceiver(broadcastReceiver, filter);
        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        params[0].registerReceiver(broadcastReceiver, filter);

        return null;
    }

    /**
     *
     */
    private class ReportFoundDevicesTask extends AsyncTask<ArrayList<String>, Void, Boolean> {

        private final String TAG = "REPORT_FOUND_DEVICES";

        @SafeVarargs
        @Override
        protected final Boolean doInBackground(ArrayList<String>... params) {
            assert params != null;
            Log.d(TAG, "doInBackground");
            // check available internet connection
            return (ClientManager.isConnected() || ClientManager.enableWifi()) && reportFoundDevices(params[0]);
        }

        private boolean reportFoundDevices(ArrayList<String> devices) {
            Log.d(TAG, "reportFoundDevices");

            try {

                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(ClientManager.getServerURL());

                List nameValuePairs = new ArrayList<BasicNameValuePair>();
                //put here in nameValuePairs request parameters
                nameValuePairs.add(new BasicNameValuePair("POST_TYPE", "RLD"));
                nameValuePairs.add(new BasicNameValuePair("dataCount", String.valueOf(devices.size())));

                int count = 0;
                for (String device : devices) {
                    nameValuePairs.add(new BasicNameValuePair("device" + count, device));
                }

                UrlEncodedFormEntity form = new UrlEncodedFormEntity(nameValuePairs, "UTF-8");
                form.setContentEncoding(HTTP.UTF_8);
                httppost.setEntity(form);

                HttpResponse response = httpclient.execute(httppost);

                Log.d(TAG, String.valueOf(response.getStatusLine().getStatusCode()));
                int status = response.getStatusLine().getStatusCode();
                if ( status == 200) {
                    Log.d(TAG, "Success");
                    return true;
                } else if (status == 500) {
                    Log.e(TAG, "Internal server error");
                    return false;
                } else {
                    Log.e(TAG, "bad argument!");
                    return  false;
                }

            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
                return false;
            }
        }
    }
}


