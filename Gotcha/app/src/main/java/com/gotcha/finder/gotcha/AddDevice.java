package com.gotcha.finder.gotcha;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class  AddDevice extends Activity {


    private BluetoothAdapter myAdapter;
    private ArrayList<GetDevice> mArrayAdapter=new ArrayList<GetDevice>();
    //CustomBaseAdapter
    private MyCustomBaseAdapter myCustomBaseAdapter;
    //List of found devices
    private ListView devicesList;
    //instance of bluetoothManager
    private bluetoothManager myManager;

    private BroadcastReceiver mReceiver;

    private IntentFilter filter;
    //For debugging
    private static String TAG = "Add_Device";

    // counter of found devices EX: device1,device2,etc
    private int counter;
    //The name entered by user to add new device
    private String Name="";
    //the Address of added device
    private String Address="";
    // The array to hold the MacAddress of the found HC-05 devices
    private Set<String> MACAddress;
    //The dataManager instance to Add new Device
    private DataManager dataManager;
    //take image for the added device
    static final int REQUEST_IMAGE_CAPTURE = 4;
    //spinner for searching
    private ProgressBar mSpinner;
    //Text view above spinner for searching
    private TextView discovery;
    //holds the current longitude if possible
    private double longitude;
    //holds the current latitude if possible
    private double latitude;
    //Location Manager to use GPS
    private LocationManager locationManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_device);
        Log.d(TAG,"on create called ");
        init();
    }

    private void init(){
        counter=1;
        MACAddress=new HashSet<String>();
        myManager=bluetoothManager.getInstance();
        myAdapter=myManager.getMyBluetoothAdapter();
        //mArrayAdapter=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, 0);
        devicesList=(ListView)findViewById(R.id.myView);

        myCustomBaseAdapter=new MyCustomBaseAdapter(this,mArrayAdapter);

        devicesList.setAdapter(myCustomBaseAdapter);

        // Get LocationManager object from System Service LOCATION_SERVICE
         locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        //try to set longitude and latitude if possible
        setLongLat();

        //show spinner and it is text at start
        mSpinner = (ProgressBar) findViewById(R.id.mySpinner);
        mSpinner.setIndeterminate(true);
        mSpinner.setVisibility(View.VISIBLE);
        discovery=(TextView) findViewById(R.id.SearchTextView);
        discovery.setVisibility(View.VISIBLE);
        devicesList.setVisibility(View.GONE);

        searchForDevices();

        dataManager=DataManager.getInstance();

        devicesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                //Add the Device to the data manager class
                EnterDeviceName(position);
            }
        });
        this.registerReceiver(mReceiver2, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
    }
    /*
        sets the current longitude and latitude if GPS was enabled
     */
    private void setLongLat() {

        if(isGPSEnabled()){
        // Create a criteria object to retrieve provider
        Criteria criteria = new Criteria();

        // Get the name of the best provider
        String provider = locationManager.getBestProvider(criteria, true);

        // Get Current Location
        Location myLocation = locationManager.getLastKnownLocation(provider);
        longitude=myLocation.getLongitude();
        latitude=myLocation.getLatitude();
        }
        else{
            //-900 is a flag that no last seen address is detected
            longitude=-900;
            latitude=-900;
        }

    }

    /*
       to check if GPS is enable or not
    */
    private boolean isGPSEnabled(){

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            return true;
        }else{
            return false;
        }
    }

    @TargetApi(Build.VERSION_CODES.ECLAIR)
    private void searchForDevices(){
        myAdapter.startDiscovery();  //start discovery to find Bluetooth Devices
        filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        mReceiver = new BroadcastReceiver() {

            @TargetApi(Build.VERSION_CODES.ECLAIR)
            @Override
            public void onReceive(Context context, Intent intent) {

                String action = intent.getAction();

                // When discovery finds a device
                if (BluetoothDevice.ACTION_FOUND.equals(action)) {

                    // Get the BluetoothDevice object from the Intent
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    // Add the name and address to an array adapter to show in a ListView
                    //add only devices with the name HC-05
                      if(device.getName().contains("HC-05")) {
                    if (!dataManager.checkForDevice((device.getAddress()))){

                        if (MACAddress.add(device.getAddress())) { //save the MAC address to the array
                            //mArrayAdapter.add(device.getName());
                            GetDevice tempDevice=new GetDevice();
                            tempDevice.setName(device.getName());
                            //add image to the device TODO add Gotcha icon instead
                            Bitmap icon = BitmapFactory.decodeResource(context.getResources(),
                                    R.drawable.bridge
                            );
                            tempDevice.setIcon(icon);


                            mArrayAdapter.add(tempDevice);
                            myCustomBaseAdapter.notifyDataSetChanged();
                        }
                        counter++; //  the next device will be for example device2,device3,etc

                    }
                    }
                }
                else if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)){
                    //hide the spinner and text textView and show listView
                    devicesList.setVisibility(View.VISIBLE);

                    mSpinner.setVisibility(View.GONE);
                    discovery.setVisibility(View.GONE);
                    //No devices Found
                    if(counter==1) {

                        NoDevicesFoundAlert();
                    }

                }
            }
        };
        // Register the BroadcastReceiver
        registerReceiver(mReceiver, filter);
        filter=new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(mReceiver, filter);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_device, menu);
        return true;
    }

    @Override
    protected void onPause(){
        super.onPause();
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        unregisterReceiver(mReceiver);
        unregisterReceiver(mReceiver2);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    //To Show Message to the user to ACK that the device has been connected
    private void showMessage(String s){

        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Device Connected");
        alertDialog.setMessage("Your " + s + " added successfully");
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //Return to Main Activity
                onBackPressed();
            }
        });
        alertDialog.show();
    }
    //Asks the user to enter a name for the new device
    private void EnterDeviceName(final int position){

        final AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Device Name");
        alert.setMessage("Enter device name");


        // Set an EditText view to get user input
        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Name = input.getText().toString();
                if(!Name.equals("")) {
                    Address = (String)MACAddress.toArray()[position];
                    if (!dataManager.checkForDevice(Address)) {
                        DisplayCameraOptions();
                    }
                }


            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.

            }
        });

        alert.show();

    }
    /*
           Check if bluetooth was turned off during the app
   */
    private final BroadcastReceiver mReceiver2 = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();

            // It means the user has changed his bluetooth state.
            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {

                if (myAdapter.getState() == BluetoothAdapter.STATE_TURNING_OFF) {
                    // The user bluetooth is turning off yet, but it is not disabled yet.
                    return;
                }

                if (myAdapter.getState() == BluetoothAdapter.STATE_OFF) {
                    // The user bluetooth is already disabled.
                    Toast.makeText(getApplicationContext(), "Gotcha is closed your Bluetooth is turned off",
                            Toast.LENGTH_LONG).show();
                    finish();
                    return;
                }

            }
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            dataManager.saveImageToInternalStorage(imageBitmap,Address,this);
        }
        
        //add the device without image
        GDevice device = new GDevice(Name, longitude,latitude, Address, "NONE", false, false);
        dataManager.addDevice(device);

        showMessage(Name);
    }
    /*
        Take image by camera for the added device
     */
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }
    /*
        Alert telling the user to take picture for the new device
        the user may press OK to take photo or just Cancel
     */
    private void DisplayCameraOptions(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set title
        alertDialogBuilder.setTitle("Assign picture");

        // set dialog message
        alertDialogBuilder
                .setMessage("Assign a photo for you new Gotcha")
                .setCancelable(false)
                .setPositiveButton("Take it",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dispatchTakePictureIntent();
                    }
                })
                .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        //Add the device without image
                        GDevice device = new GDevice(Name, longitude,latitude, Address, "NONE", false, false);
                        dataManager.addDevice(device);

                        showMessage(Name);
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    /*
    Alert the user that no new Gotcha devices were found in range
     */
    private void NoDevicesFoundAlert(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set title
        alertDialogBuilder.setTitle("No Gotcha Found");

        // set dialog message
        alertDialogBuilder
                .setMessage("Please make sure to have your new Gotcha near your phone")
                .setCancelable(false)
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    public void    onClick(DialogInterface dialog,int id) {
                        onBackPressed();
                    }
                });


        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }
    /*
        finish and return to main
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(AddDevice.this,Main.class));
        finish();
    }

}

