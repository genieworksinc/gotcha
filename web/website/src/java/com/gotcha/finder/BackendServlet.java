/*
 * Gotha™ All Rights Reserved.
 */
package com.gotcha.finder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_OK;

/**
 *
 * @author mohamed
 */
@WebServlet(name = "BackendServlet", urlPatterns = {"/BackendServlet"})
public class BackendServlet extends HttpServlet {
    
    /// logger for debuging
    private static final Logger logger = Logger.getLogger(BackendServlet.class.getName());
    
    // post
        // can be a request for:
        // 1. add lost device       => ALD
        // 2. update user email     => UUE  removed
        // 3. update user password  => UUP  removed
        // 4. add new user          => AUA  removed
        // 5. delete user           => DLU  removed
        // 6. delete lost device    => DLD
        // 7. get lost devices
        // 8. login                 => LGN  removed
    
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        // we have no get requests availabe
        response.setContentType("text/html;charset=UTF-8");
        response.setStatus(SC_OK);
        response.getOutputStream().println("GOTCHA!");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String type = request.getParameter("POST_TYPE");
        
        if (type == null) {
            System.out.println("type is null!");
            response.setHeader("Content-Type", "text/plain");
            response.setStatus(SC_BAD_REQUEST);
            return;
        }
        
        switch (type) {
            case "ALD":
                processALD(request, response);
                break;
            /*case "UUE":
                processUUE(request, response);
                break;
            case "UUP":
                processUUP(request, response);
                break;
            case "AUA":
                processAUA(request, response);
                break;
            case "DLU":
                processDLU(request, response);
                break;*/
            case "DLD":
                processDLD(request, response);
                break;
            /*case "LGN":
                processLogin(request, response);
                break;*/
                
            default:
                process(request, response);
                break;
        }
    }

    /**
     * Process an add lost device request.
     * @param request servlet request
     * @param response servlet response
     */
    private void processALD(HttpServletRequest request, HttpServletResponse response) {
        // extrcat parameters
        String deviceAddress = request.getParameter("deviceAddress");
        String deviceName = request.getParameter("deviceName");
        String lastAddress = request.getParameter("deviceLastAddress");
        
        String email = request.getParameter("email");
        
        if (deviceAddress == null || deviceName == null || lastAddress == null
                || email == null) {
            response.setStatus(SC_BAD_REQUEST);
            return;
        }
        
        // send parameters to remote server
        if (DBHelper.addLostDevice(email, deviceAddress, deviceName, lastAddress)) {
            response.setStatus(SC_OK);
        } else {
            response.setStatus(SC_INTERNAL_SERVER_ERROR);
        }
    }
    
    /**
     * Process an update user email request
     * @param request servlet request
     * @param response servlet response
     *
    private void processUUE(HttpServletRequest request, HttpServletResponse response) {
        String email = request.getParameter("email");
        
        String suserID = request.getParameter("userID");
        
        if (email == null || suserID == null) {
            response.setStatus(SC_BAD_REQUEST);
            return;
        }
        
        int userID = Integer.valueOf(suserID);
        
        // send parameters to remote server
        if (DBHelper.updateUserEmail(userID, email)) {
            response.setStatus(SC_OK);
        } else {
            response.setStatus(SC_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Process an update user password request.
     * @param request servlet request
     * @param response servlet response
     *
    private void processUUP(HttpServletRequest request, HttpServletResponse response) {
        String password = request.getParameter("password");
        String suserID = request.getParameter("userID");
        
        if (password == null || suserID == null) {
            response.setStatus(SC_BAD_REQUEST);
            return;
        }
        
        int userID = Integer.valueOf(suserID);
        
        // send parameters to remote server
        if (DBHelper.updateUserPassword(userID, password)) {
            response.setStatus(SC_OK);
        } else {
            response.setStatus(SC_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Process a register user request.
     * @param request servlet request
     * @param response servlet response
     *
    private void processAUA(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // extrcat parameters
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        
        if (email == null || password == null) {
            
            response.setStatus(SC_BAD_REQUEST);
            return;
        }
        
        Integer userID = -1;
        
        // send parameters to remote server
        if (DBHelper.addNewUser(userID, email, password)) {
            response.setStatus(SC_OK);
            response.setHeader("Content-Type", "text/plain");
            
            // send back the user id
            response.getOutputStream().println(userID);
        } else {
            response.setStatus(SC_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Process a delete user request.
     * @param request servlet request
     * @param response servlet response
     *
    private void processDLU(HttpServletRequest request, HttpServletResponse response) {
        String suserID = request.getParameter("userID");
        
        if (suserID == null) {
            response.setStatus(SC_BAD_REQUEST);
            return;
        }
        
        int userID = Integer.valueOf(suserID);
        
        // send parameters to remote server
        if (DBHelper.deleteUser(userID)) {
            response.setStatus(SC_OK);
        } else {
            response.setStatus(SC_INTERNAL_SERVER_ERROR);
        }
    }*/

    /**
     * Process a delete lost device request.
     * @param request servlet request
     * @param response servlet response
     */
    private void processDLD(HttpServletRequest request, HttpServletResponse response) {
        String address = request.getParameter("deviceAddress");
        
        if (address == null) {
            response.setStatus(SC_BAD_REQUEST);
            return;
        }
        
        // send parameters to remote server
        if (DBHelper.deleteDevice(address)) {
            response.setStatus(SC_OK);
        } else {
            response.setStatus(SC_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Process a get lost devices request.
     * @param request servlet request
     * @param response servlet response
     */
    private void process(HttpServletRequest request, HttpServletResponse response) {
        String dataCount = request.getParameter("dataCount");
        
        if (dataCount == null) {
            response.setStatus(SC_BAD_REQUEST);
            return;
        }
        
        int count = Integer.valueOf(dataCount);
        ArrayList<String> devices = new ArrayList<>();
        String device;
        for (int i=0; i<count; i++) {
            device = request.getParameter("device" + i);
            
            if (device == null) {
                response.setStatus(SC_BAD_REQUEST);
                return;
            }
            
            devices.add(device);
        }
        
        if (DBHelper.getLostDevicesByLocation(devices)) {
            response.setStatus(SC_OK);
        } else {
            response.setStatus(SC_INTERNAL_SERVER_ERROR);
        }
    }
    
    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "backend for the gotcha application";
    }

    /*
    private void processLogin(HttpServletRequest request, HttpServletResponse response) {
        String email    = request.getParameter("email");
        String password = request.getParameter("password");
        
        if (email == null || password == null) {
            response.setStatus(SC_BAD_REQUEST);
            return;
        }
        
        Integer userID = -1;
        
        // it is not the server responsiblity to figure out that its a failed
        // atempt to login, ie: the return value of userID is the client own
        // bussiness. Server only process requests blindly.
        if (DBHelper.login(userID, email, password)) {
           response.setStatus(SC_OK);
        } else {
            response.setStatus(SC_INTERNAL_SERVER_ERROR);
        }
    }*/
}
