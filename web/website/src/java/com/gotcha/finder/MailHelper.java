/*
 * Gotha™ All Rights Reserved.
 */
package com.gotcha.finder;

import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Send emails to users
 * @author mohamed
 */
public class MailHelper {
    public static boolean sendEmail(String email, String name, String address) {
        // login id for gotcha mail account
        String login = "gotcha.no.reply@gmail.com";
        
        // password for gotcha mail account
        String password = "gotcha1984";

        // Assuming you are sending email from localhost
        //String host = "smtp.gmail.com";

        // Get system properties
        Properties properties = System.getProperties();

        // Setup mail server
        properties.setProperty("mail.host", "smtp.gmail.com");
        properties.setProperty("mail.smtp.port", "587");
        properties.setProperty("mail.smtp.auth", "true");
        properties.setProperty("mail.smtp.starttls.enable", "true");

        
        Authenticator auth = new SMTPAuthenticator(login, password);
        
        // Get the default Session object.
        Session session = Session.getInstance(properties, auth);

        try{
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO,
                                     new InternetAddress(email));

            // Set Subject: header field
            message.setSubject("Lost device was found");

            // Now set the actual message
            StringBuilder messageBuilder = new StringBuilder();
            messageBuilder.append("your ").append(name).append(" with mac-address:")
                    .append(address).append(" that was reported as lost was found!");
            message.setText(messageBuilder.toString());

            // Send message
            Transport.send(message);
            System.out.println("Sent message successfully....");

            return true;
        }catch (MessagingException mex) {
            System.out.println(mex.getMessage());
            return false;
        }
   }
    
    private static class SMTPAuthenticator extends Authenticator {
        private final PasswordAuthentication authentication;

        public SMTPAuthenticator(String login, String password) {
            authentication = new PasswordAuthentication(login, password);
        }

        @Override
        protected PasswordAuthentication getPasswordAuthentication() {
            return authentication;
        }
    }
}

