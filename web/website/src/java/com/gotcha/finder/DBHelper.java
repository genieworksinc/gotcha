/*
 * Gotha™ All Rights Reserved.
 */
package com.gotcha.finder;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mohamed moanis
 */
public class DBHelper {
    
    /**
     * Reports a new lost device.
     * @param email user's email
     * @param deviceAddress mac-address of the device
     * @param deviceName name of device
     * @param lastSeenAddress last know address before device was missing
     * @return true on success, false otherwise.
     */
    public static boolean addLostDevice(String email, String deviceAddress, String deviceName, String lastSeenAddress) {
        StringBuilder queryBuilder = new StringBuilder();
        
        try {
            if (connection == null && createDatabaseConnection() == false)
                return false;
            
            // prepare sql query
            queryBuilder.append("Insert Into LostDevices(EmailAddress, DeviceMAC, DeviceName, LastKnownAddress) ")
                        .append("values('")
                        .append(email).append("', '")
                        .append(deviceAddress).append("', '")
                        .append(deviceName).append("', '")
                        .append(lastSeenAddress).append("')");
            
            
            try (java.sql.Statement stat = connection.createStatement()) {
                stat.executeUpdate(queryBuilder.toString());
                return true;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, queryBuilder.toString());
            logger.log(Level.SEVERE, ex.getMessage());
            return false;
        }
    }

    /**
     * Register a new user.
     * @param userid empty integer, will be equal to user if (access token) after
     *  method return
     * @param email user email address
     * @param password user password
     * @return true on success, false otherwise.
     *
    public static boolean addNewUser(Integer userid, String email, String password) {
        StringBuilder queryBuilder = new StringBuilder();
        try {
            if (connection == null && createDatabaseConnection() == false)
                return false;
            
            // prepare sql query
            queryBuilder.append("Insert Into USER_(Email_Address,PassWord) ")
                        .append("values('")
                        .append(email).append("', '")
                        .append(password).append("')");
            
            try (java.sql.Statement stat = connection.createStatement()) {
                stat.executeUpdate(queryBuilder.toString());
                
                // TODO: recheck that query
                queryBuilder.setLength(0);
                queryBuilder.append("SELECT UserID FROM USER_ where Email_Address='")
                        .append(email).append("'");
                
                // get user id
                ResultSet results = stat.executeQuery(queryBuilder.toString());
                if (results.next()) {
                    userid = results.getInt(1);
                } else {
                    userid = 0;
                }
                return true;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, queryBuilder.toString());
            logger.log(Level.SEVERE, ex.getMessage());
            return false;
        }
    }*/

    /**
     * Remove a reported lost device.
     * @param deviceAddress lost device mac-address
     * @return true on success, false otherwise.
     */
    public static boolean deleteDevice(String deviceAddress) {
        StringBuilder queryBuilder = new StringBuilder();
        
        try {
            if (connection == null && createDatabaseConnection() == false)
                return false;
            
            // prepare sql query
            queryBuilder.append("DELETE FROM LostDevices WHERE DeviceMAC='")
                        .append(deviceAddress).append("'");
            
            
            try (java.sql.Statement stat = connection.createStatement()) {
                stat.executeUpdate(queryBuilder.toString());
                return true;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, queryBuilder.toString());
            logger.log(Level.SEVERE, ex.getMessage());
            return false;
        }
    }

    /**
     * Remove a registered user.
     * @param userID access token of the user
     * @return true on success, false otherwise.
     *
    public static boolean deleteUser(int userID) {
        StringBuilder queryBuilder = new StringBuilder();
        
        try {
            if (connection == null && createDatabaseConnection() == false)
                return false;
            
            // prepare sql query
            queryBuilder.append("DELETE FROM USER_ WHERE UserID=")
                        .append(userID);
            
            
            try (java.sql.Statement stat = connection.createStatement()) {
                stat.executeUpdate(queryBuilder.toString());
                return true;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, queryBuilder.toString());
            logger.log(Level.SEVERE, ex.getMessage());
            return false;
        }
    }*/
    
    /**
     * Check for a lost device found by certain user.
     * @param devices list of found devices
     * @return true on success, false otherwise
     */
    public static boolean getLostDevicesByLocation(ArrayList<String> devices) {
        StringBuilder queryBuilder = new StringBuilder();
        
        try {
            if (connection == null && createDatabaseConnection() == false)
                return false;
            
            for (String device : devices) {
                // TODO: prepare sql query
                queryBuilder.append("SELECT EmailAddress, DeviceName FROM LostDevices WHERE ")
                        .append("DeviceMAC = '").append(device).append("'");
                try (java.sql.Statement stat = connection.createStatement()) {
                    ResultSet result = stat.executeQuery(queryBuilder.toString());
                    
                    // check result
                    if (result.next()) {
                        // send an email to notify user
                        MailHelper.sendEmail(result.getString(1), 
                                result.getString(2), device);
                    }
                    return true;
                }
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, queryBuilder.toString());
            logger.log(Level.SEVERE, ex.getMessage());
            return false;
        }
        
        // TODO: implement method
        return false;
    }
    
    /**
     * Login by fetching userid for a given user's email and password.
     * @param userID user access token
     * @param email user's email
     * @param password user's password
     * @return true on success, false otherwise
     *
    public static boolean login(Integer userID, String email, String password) {
        StringBuilder queryBuilder = new StringBuilder();
        
        try {
            if (connection == null && createDatabaseConnection() == false)
                return false;
            
            // prepare sql query
            queryBuilder.append("SELECT UserID FROM USER_ WHERE Email_Address='")
                        .append(email).append("' AND password='")
                        .append(password).append("'");
            
            try (java.sql.Statement stat = connection.createStatement()) {
                ResultSet result = stat.executeQuery(queryBuilder.toString());
                if (result.next()) {
                    userID = result.getInt(1);
                } else {
                    userID = -1;
                }
                return true;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, queryBuilder.toString());
            logger.log(Level.SEVERE, ex.getMessage());
            return false;
        }
    }

    /**
     * Update a registered user email.
     * @param userID access token of the user
     * @param password user's new password
     * @return true on success, false otherwise.
     *
    public static boolean updateUserEmail(int userID, String password) {
        try {
            if (connection == null && createDatabaseConnection() == false)
                return false;
            
            // prepare sql query
            StringBuilder queryBuilder = new StringBuilder();
            queryBuilder.append("Update USER_ SET Email_Address = '")
                        .append(password)
                        .append("' where UserID=")
                        .append(userID);
            
            
            try (java.sql.Statement stat = connection.createStatement()) {
                stat.executeUpdate(queryBuilder.toString());
                return true;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, ex.getMessage());
            return false;
        }
    }

    /**
     * Update a registered user password.
     * @param userID access token of the user
     * @param password user's new password
     * @return true on success, false otherwise.
     *
    public static boolean updateUserPassword(int userID, String password) {
        StringBuilder queryBuilder = new StringBuilder();
        try {
            if (connection == null && createDatabaseConnection() == false)
                return false;
            
            // prepare sql query
            queryBuilder.append("Update USER_ SET password='")
                        .append(password)
                        .append("' where UserID=")
                        .append(userID);
            
            
            try (java.sql.Statement stat = connection.createStatement()) {
                stat.executeUpdate(queryBuilder.toString());
                return true;
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, queryBuilder.toString());
            logger.log(Level.SEVERE, e.getMessage());
            return false;
        }
    }*/
    
    /**
     * Initializes a connection to the database.
     * @return true on success, false otherwise
     */
    private static boolean createDatabaseConnection() {
        try
        {
            Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
            //Get a connection
            connection = DriverManager.getConnection(databaseURL);
            return true;
        }
        catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e)
        {
            logger.log(Level.SEVERE, e.getMessage());
            return false;
        }
    }
    
    // Connection string
    /*TODO: supply the name of schema, username, password according to the running server host*/
    private static final String databaseURL = "jdbc:derby://localhost:1527/gotcha;create=true;user=moanis;password=123";
    
    // jdbc Connection
    private static Connection connection = null;
    
    /// for debuging
    private static final Logger logger = Logger.getLogger(DBHelper.class.getName());
}
